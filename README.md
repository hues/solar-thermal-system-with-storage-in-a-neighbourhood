# README #


## What is this repository for? ##

* This repository consists an EnergyPlus model of solar thermal system with storage in a neighbourhood of 11 buildings. The solar collectors supplying building space heating demand are either placed centrally or at building level with centralised long-term storage, however, the solar collectors supplying Domestic Hot Water are placed at individual buildings with short-term storage.


* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up? ##

Place the "ExampleFiles folder" folder to the following directory: "C:\EnergyPlusV8-1-0\ExampleFiles\Configurations\"

### - Loops supplying SH Demand: Decentralized production+ Centralized storage.###
Files with the same model but different outputs:
Collector output variables: D_CL_collectors_all_smallpipetest.imf;
Pipe output variables: D_CL_Pipes_smallpipetest.imf;
Pump output variables: D_CL_Pumps_smallpipetest.imf;  
Water heater output variables: D_CL_waterheater_smallpipetest.imf; 

### - Loops supplying SH Demand:Centralized production+ Centralized storage.###
  Files with the same model but different outputs:Collector output variables: C_CL_centralcollectors_smallpipetest_10m.imf;
  Pipe output variables: C_CL_pipes_smallpipetest_10m.imf;
  Pump output variables: C_CL_pumps_smallpipetest_10m.imf; 
  Water heater output variables: C_CL_waterheater_smallpipetest_10m.imf;

### - Loops supplying DHW Demand: Decentralized production+ decentralized storage.###
  Files with the same model but different outputs: DS.imf;
  DS_2.imf;