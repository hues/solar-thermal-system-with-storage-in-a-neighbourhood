!====================B3 Zone Radiant Branch ========================

!***********Branch B3-1******************
Branch,
    B3-1 Zone Radiant Branch,  !- Name
    0,                       !- Maximum Flow Rate {m3/s}
    ,                        !- Pressure Drop Curve Name
    ZoneHVAC:LowTemperatureRadiant:VariableFlow,  !- Component 1 Object Type
    B3-1 Zone Radiant Floor,  !- Component 1 Name
    B3-1 Zone Radiant Water Inlet Node,  !- Component 1 Inlet Node Name
    B3-1 Zone Radiant Water Outlet Node,  !- Component 1 Outlet Node Name
    ACTIVE;                  !- Component 1 Branch Control Type

ZoneHVAC:LowTemperatureRadiant:VariableFlow,
    B3-1 Zone Radiant Floor,  !- Name
    RadiantSysAvailSched,    !- Availability Schedule Name
    B3-1 Zone,        !- Zone Name
    Surface B3 5,     !- Surface Name or Radiant Surface Group Name
    0.013,                   !- Hydronic Tubing Inside Diameter {m}
    autosize,                !- Hydronic Tubing Length {m}
    MeanAirTemperature,      !- Temperature Control Type
    autosize,                !- Maximum Hot Water Flow {m3/s}
    B3-1 Zone Radiant Water Inlet Node,  !- Heating Water Inlet Node Name
    B3-1 Zone Radiant Water Outlet Node,  !- Heating Water Outlet Node Name
    2.0,                     !- Heating Control Throttling Range {deltaC}
    Radiant Heating Setpoints,  !- Heating Control Temperature Schedule Name
    0,                       !- Maximum Cold Water Flow {m3/s}
    ,                        !- Cooling Water Inlet Node Name
    ,                        !- Cooling Water Outlet Node Name
    ,                        !- Cooling Control Throttling Range {deltaC}
    ,                        !- Cooling Control Temperature Schedule Name
    ,                        !- Condensation Control Type
    ,                        !- Condensation Control Dewpoint Offset {C}
    ,                        !- Number of Circuits
    ;                        !- Circuit Length {m}
    
ZoneHVAC:EquipmentList,
    B3-1 Zone Equipment,          !- Name
    ZoneHVAC:LowTemperatureRadiant:VariableFlow,  !- Zone Equipment 1 Object Type
    B3-1 Zone Radiant Floor,  !- Zone Equipment 1 Name
    1,                       !- Zone Equipment 1 Cooling Sequence
    1;                       !- Zone Equipment 1 Heating or No-Load Sequence

ZoneHVAC:EquipmentConnections,
    B3-1 Zone,        !- Zone Name
    B3-1 Zone Equipment,          !- Zone Conditioning Equipment List Name
    ,                        !- Zone Air Inlet Node or NodeList Name
    ,                        !- Zone Air Exhaust Node or NodeList Name
    B3-1 Zone Node,             !- Zone Air Node Name
    B3-1 Zone Outlet Node;      !- Zone Return Air Node Name

ZoneControl:Thermostat,
    B3-1 Zone Thermostat,       !- Name
    B3-1 Zone,        !- Zone or ZoneList Name
    Zone Control Type Sched, !- Control Type Schedule Name
    ThermostatSetpoint:SingleHeating,  !- Control 1 Object Type
    Heating Setpoint with SB; !- Control 1 Name
    
!***********Branch B3-2******************
Branch,
    B3-2 Zone Radiant Branch,  !- Name
    0,                       !- Maximum Flow Rate {m3/s}
    ,                        !- Pressure Drop Curve Name
    ZoneHVAC:LowTemperatureRadiant:VariableFlow,  !- Component 1 Object Type
    B3-2 Zone Radiant Floor,  !- Component 1 Name
    B3-2 Zone Radiant Water Inlet Node,  !- Component 1 Inlet Node Name
    B3-2 Zone Radiant Water Outlet Node,  !- Component 1 Outlet Node Name
    ACTIVE;                  !- Component 1 Branch Control Type

ZoneHVAC:LowTemperatureRadiant:VariableFlow,
    B3-2 Zone Radiant Floor,  !- Name
    RadiantSysAvailSched,    !- Availability Schedule Name
    B3-2 Zone,        !- Zone Name
    Surface B3 46,     !- Surface Name or Radiant Surface Group Name
    0.013,                   !- Hydronic Tubing Inside Diameter {m}
    autosize,                !- Hydronic Tubing Length {m}
    MeanAirTemperature,      !- Temperature Control Type
    autosize,                !- Maximum Hot Water Flow {m3/s}
    B3-2 Zone Radiant Water Inlet Node,  !- Heating Water Inlet Node Name
    B3-2 Zone Radiant Water Outlet Node,  !- Heating Water Outlet Node Name
    2.0,                     !- Heating Control Throttling Range {deltaC}
    Radiant Heating Setpoints,  !- Heating Control Temperature Schedule Name
    0,                       !- Maximum Cold Water Flow {m3/s}
    ,                        !- Cooling Water Inlet Node Name
    ,                        !- Cooling Water Outlet Node Name
    ,                        !- Cooling Control Throttling Range {deltaC}
    ,                        !- Cooling Control Temperature Schedule Name
    ,                        !- Condensation Control Type
    ,                        !- Condensation Control Dewpoint Offset {C}
    ,                        !- Number of Circuits
    ;                        !- Circuit Length {m}
    
ZoneHVAC:EquipmentList,
    B3-2 Zone Equipment,          !- Name
    ZoneHVAC:LowTemperatureRadiant:VariableFlow,  !- Zone Equipment 1 Object Type
    B3-2 Zone Radiant Floor,  !- Zone Equipment 1 Name
    1,                       !- Zone Equipment 1 Cooling Sequence
    1;                       !- Zone Equipment 1 Heating or No-Load Sequence

ZoneHVAC:EquipmentConnections,
    B3-2 Zone,        !- Zone Name
    B3-2 Zone Equipment,          !- Zone Conditioning Equipment List Name
    ,                        !- Zone Air Inlet Node or NodeList Name
    ,                        !- Zone Air Exhaust Node or NodeList Name
    B3-2 Zone Node,             !- Zone Air Node Name
    B3-2 Zone Outlet Node;      !- Zone Return Air Node Name

ZoneControl:Thermostat,
    B3-2 Zone Thermostat,       !- Name
    B3-2 Zone,        !- Zone or ZoneList Name
    Zone Control Type Sched, !- Control Type Schedule Name
    ThermostatSetpoint:SingleHeating,  !- Control 1 Object Type
    Heating Setpoint with SB; !- Control 1 Name


!**********************Zone Control Schedule*********************
Schedule:Compact,
    RADIANTSYSAVAILSCHED,    !- Name
    FRACTION,                !- Schedule Type Limits Name
    Through: 12/31,          !- Field 1
    For: Alldays,            !- Field 2
    Until: 24:00, 1.00;      !- Field 4

Schedule:Compact,
    RADIANT HEATING SETPOINTS,  !- Name
    TEMPERATURE,             !- Schedule Type Limits Name
    Through: 12/31,          !- Field 1
    For: Alldays,            !- Field 2
    Until: 7:00, 12.00,      !- Field 4
    Until: 17:00, 17.00,     !- Field 6
    Until: 24:00, 12.00;     !- Field 8


Schedule:Compact,
    ZONE CONTROL TYPE SCHED, !- Name
    CONTROL TYPE,            !- Schedule Type Limits Name
    Through: 3/31,           !- Field 1
    For: Alldays,            !- Field 2
    Until: 24:00, 1,         !- Field 4
    Through: 9/30,           !- Field 5
    For: Alldays,            !- Field 6
    Until: 24:00, 0,         !- Field 8
    Through: 12/31,          !- Field 9
    For: Alldays,            !- Field 10
    Until: 24:00, 1;         !- Field 12

ThermostatSetpoint:SingleHeating,
    Heating Setpoint with SB,!- Name
    Heating Setpoints;       !- Setpoint Temperature Schedule Name

Schedule:Compact,
    HEATING SETPOINTS,       !- Name
    TEMPERATURE,             !- Schedule Type Limits Name
    Through: 12/31,          !- Field 1
    For: Weekdays Weekends Holidays CustomDay1 CustomDay2,  !- Field 2
    Until: 7:00, 15.00,      !- Field 4
    Until: 17:00, 20.00,     !- Field 6
    Until: 24:00, 15.00,     !- Field 8
    For: SummerDesignDay,    !- Field 9
    Until: 24:00, 15.00,     !- Field 11
    For: WinterDesignDay,    !- Field 12
    Until: 24:00, 20.00;     !- Field 14


!=============================Zone Information==============================================
! original zone origin (0,0,0)
##fileprefix C:\EnergyPlusV8-1-0\ExampleFiles\Configurations\
##include ZoneSchedules.idf

People,
    B3-1 People,      !- Name
    B3-1 Zone,        !- Zone or ZoneList Name
    Residential Occupancy,   !- Number of People Schedule Name
    People,                  !- Number of People Calculation Method
    3,                       !- Number of People
    ,                        !- People per Zone Floor Area {person/m2}
    ,                        !- Zone Floor Area per Person {m2/person}
    0.3,                     !- Fraction Radiant
    autocalculate,           !- Sensible Heat Fraction
    Activity Sch,            !- Activity Level Schedule Name
    0,                       !- Carbon Dioxide Generation Rate {m3/s-W}
    No,                      !- Enable ASHRAE 55 Comfort Warnings
    ZoneAveraged,            !- Mean Radiant Temperature Calculation Type
    ,                        !- Surface Name/Angle Factor List Name
    ,                        !- Work Efficiency Schedule Name
    Clothing Sch;            !- Clothing Insulation Calculation Method
    
People,
    B3-2 People,      !- Name
    B3-2 Zone,        !- Zone or ZoneList Name
    Residential Occupancy,   !- Number of People Schedule Name
    People,                  !- Number of People Calculation Method
    4,                       !- Number of People
    ,                        !- People per Zone Floor Area {person/m2}
    ,                        !- Zone Floor Area per Person {m2/person}
    0.3,                     !- Fraction Radiant
    autocalculate,           !- Sensible Heat Fraction
    Activity Sch,            !- Activity Level Schedule Name
    0,                       !- Carbon Dioxide Generation Rate {m3/s-W}
    No,                      !- Enable ASHRAE 55 Comfort Warnings
    ZoneAveraged,            !- Mean Radiant Temperature Calculation Type
    ,                        !- Surface Name/Angle Factor List Name
    ,                        !- Work Efficiency Schedule Name
    Clothing Sch;            !- Clothing Insulation Calculation Method

ElectricEquipment,
    B3-1 Zone ElecEq 1,  !- Name
    B3-1 Zone,        !- Zone or ZoneList Name
    Intermittent,            !- Schedule Name
    EquipmentLevel,          !- Design Level Calculation Method
    1228,                !- Design Level {W}
    ,                        !- Watts per Zone Floor Area {W/m2}
    ,                        !- Watts per Person {W/person}
    0,                       !- Fraction Latent
    0.3000000,               !- Fraction Radiant
    0;                       !- Fraction Lost

ElectricEquipment,
    B3-2 Zone ElecEq 1,  !- Name
    B3-2 Zone,        !- Zone or ZoneList Name
    Intermittent,            !- Schedule Name
    EquipmentLevel,          !- Design Level Calculation Method
    1228,                !- Design Level {W}
    ,                        !- Watts per Zone Floor Area {W/m2}
    ,                        !- Watts per Person {W/person}
    0,                       !- Fraction Latent
    0.3000000,               !- Fraction Radiant
    0;                       !- Fraction Lost

Lights,
    B3-1,             !- Name
    B3-1 Zone,        !- Zone or ZoneList Name
    Residential Room Lighting,  !- Schedule Name
    LightingLevel,           !- Design Level Calculation Method
    1833,                  !- Lighting Level {W}
    ,                        !- Watts per Zone Floor Area {W/m2}
    ,                        !- Watts per Person {W/person}
    0.2,                     !- Return Air Fraction
    0.59,                    !- Fraction Radiant
    0.2,                     !- Fraction Visible
    0,                       !- Fraction Replaceable
    General,                 !- End-Use Subcategory
    No;                      !- Return Air Fraction Calculated from Plenum Temperature
    
Lights,
    B3-2,             !- Name
    B3-2 Zone,        !- Zone or ZoneList Name
    Residential Room Lighting,  !- Schedule Name
    LightingLevel,           !- Design Level Calculation Method
    1833,                  !- Lighting Level {W}
    ,                        !- Watts per Zone Floor Area {W/m2}
    ,                        !- Watts per Person {W/person}
    0.2,                     !- Return Air Fraction
    0.59,                    !- Fraction Radiant
    0.2,                     !- Fraction Visible
    0,                       !- Fraction Replaceable
    General,                 !- End-Use Subcategory
    No;                      !- Return Air Fraction Calculated from Plenum Temperature


ZoneInfiltration:DesignFlowRate,
    B3-1 Zone Infil 1,!- Name
    B3-1 Zone,        !- Zone or ZoneList Name
    ON,                      !- Schedule Name
    flow/zone,               !- Design Flow Rate Calculation Method
    ,                   !- Design Flow Rate {m3/s}
    ,                        !- Flow per Zone Floor Area {m3/s-m2}
    ,                        !- Flow per Exterior Surface Area {m3/s-m2}
    0.3,                        !- Air Changes per Hour {1/hr}
    1.0,                     !- Constant Term Coefficient
    0.0,                     !- Temperature Term Coefficient
    0.0,                     !- Velocity Term Coefficient
    0.0;                     !- Velocity Squared Term Coefficient
    
ZoneInfiltration:DesignFlowRate,
    B3-2 Zone Infil 1,!- Name
    B3-2 Zone,        !- Zone or ZoneList Name
    ON,                      !- Schedule Name
    flow/zone,               !- Design Flow Rate Calculation Method
    ,                   !- Design Flow Rate {m3/s}
    ,                        !- Flow per Zone Floor Area {m3/s-m2}
    ,                        !- Flow per Exterior Surface Area {m3/s-m2}
    0.3,                        !- Air Changes per Hour {1/hr}
    1.0,                     !- Constant Term Coefficient
    0.0,                     !- Temperature Term Coefficient
    0.0,                     !- Velocity Term Coefficient
    0.0;                     !- Velocity Squared Term Coefficient

Schedule:Compact,
    ON,                      !- Name
    Fraction,                !- Schedule Type Limits Name
    Through: 12/31,          !- Field 1
    For: Alldays,            !- Field 2
    Until: 24:00, 1.00;      !- Field 4

Sizing:Zone,
    B3-1 Zone,        !- Zone or ZoneList Name
    SupplyAirTemperature,    !- Zone Cooling Design Supply Air Temperature Input Method
    16.,                     !- Zone Cooling Design Supply Air Temperature {C}
    ,                        !- Zone Cooling Design Supply Air Temperature Difference {deltaC}
    SupplyAirTemperature,    !- Zone Heating Design Supply Air Temperature Input Method
    40.,                     !- Zone Heating Design Supply Air Temperature {C}
    ,                        !- Zone Heating Design Supply Air Temperature Difference {deltaC}
    0.009,                   !- Zone Cooling Design Supply Air Humidity Ratio {kgWater/kgDryAir}
    0.004,                   !- Zone Heating Design Supply Air Humidity Ratio {kgWater/kgDryAir}
    SZ DSOA B3-1 Zone,!- Design Specification Outdoor Air Object Name
    0.0,                     !- Zone Heating Sizing Factor
    0.0,                     !- Zone Cooling Sizing Factor
    DesignDay,               !- Cooling Design Air Flow Method
    0,                       !- Cooling Design Air Flow Rate {m3/s}
    ,                        !- Cooling Minimum Air Flow per Zone Floor Area {m3/s-m2}
    ,                        !- Cooling Minimum Air Flow {m3/s}
    ,                        !- Cooling Minimum Air Flow Fraction
    DesignDay,               !- Heating Design Air Flow Method
    0;                       !- Heating Design Air Flow Rate {m3/s}

    DesignSpecification:OutdoorAir,
    SZ DSOA B3-1 Zone,!- Name
    flow/person,             !- Outdoor Air Method
    0.00944,                 !- Outdoor Air Flow per Person {m3/s-person}
    0.0,                     !- Outdoor Air Flow per Zone Floor Area {m3/s-m2}
    0.0;                     !- Outdoor Air Flow per Zone {m3/s}
    
Sizing:Zone,
    B3-2 Zone,        !- Zone or ZoneList Name
    SupplyAirTemperature,    !- Zone Cooling Design Supply Air Temperature Input Method
    16.,                     !- Zone Cooling Design Supply Air Temperature {C}
    ,                        !- Zone Cooling Design Supply Air Temperature Difference {deltaC}
    SupplyAirTemperature,    !- Zone Heating Design Supply Air Temperature Input Method
    40.,                     !- Zone Heating Design Supply Air Temperature {C}
    ,                        !- Zone Heating Design Supply Air Temperature Difference {deltaC}
    0.009,                   !- Zone Cooling Design Supply Air Humidity Ratio {kgWater/kgDryAir}
    0.004,                   !- Zone Heating Design Supply Air Humidity Ratio {kgWater/kgDryAir}
    SZ DSOA B3-2 Zone,!- Design Specification Outdoor Air Object Name
    0.0,                     !- Zone Heating Sizing Factor
    0.0,                     !- Zone Cooling Sizing Factor
    DesignDay,               !- Cooling Design Air Flow Method
    0,                       !- Cooling Design Air Flow Rate {m3/s}
    ,                        !- Cooling Minimum Air Flow per Zone Floor Area {m3/s-m2}
    ,                        !- Cooling Minimum Air Flow {m3/s}
    ,                        !- Cooling Minimum Air Flow Fraction
    DesignDay,               !- Heating Design Air Flow Method
    0;                       !- Heating Design Air Flow Rate {m3/s}

    DesignSpecification:OutdoorAir,
    SZ DSOA B3-2 Zone,!- Name
    flow/person,             !- Outdoor Air Method
    0.00944,                 !- Outdoor Air Flow per Person {m3/s-person}
    0.0,                     !- Outdoor Air Flow per Zone Floor Area {m3/s-m2}
    0.0;                     !- Outdoor Air Flow per Zone {m3/s}

!=============================Building Construction==============================================
Building,
    Building 3,              !- Name
    0.0,                     !- North Axis {deg}
    Suburbs,                 !- Terrain
    0.04,                    !- Loads Convergence Tolerance Value
    0.04,                    !- Temperature Convergence Tolerance Value {deltaC}
    FullInteriorAndExterior, !- Solar Distribution
    25,                      !- Maximum Number of Warmup Days
    6;                       !- Minimum Number of Warmup Days

!======================== Building 3 Surfaces ==========================================


Zone,
  B3-1 Zone,                      !- Name
  -0,                                     !- Direction of Relative North {deg}
  0,                                      !- X Origin {m}
  0,                                      !- Y Origin {m}
  0,                                      !- Z Origin {m}
  ,                                       !- Type
  ,                                       !- Multiplier
  ,                                       !- Ceiling Height {m}
  ,                                       !- Volume {m3}
  ,                                       !- Floor Area {m2}
  ,                                       !- Zone Inside Convection Algorithm
  ;                                       !- Zone Outside Convection Algorithm

BuildingSurface:Detailed,
  Surface B3 1,                              !- Name
  Wall,                                   !- Surface Type
  Wall, !- Construction Name
  B3-1 Zone,                      !- Zone Name
  Outdoors,                               !- Outside Boundary Condition
  ,                                       !- Outside Boundary Condition Object
  SunExposed,                             !- Sun Exposure
  WindExposed,                            !- Wind Exposure
  ,                                       !- View Factor to Ground
  ,                                       !- Number of Vertices
  99.6025163485372, 139.882804258008, 2.8, !- X,Y,Z Vertex 1 {m}
  99.6025163485372, 139.882804258008, 0,  !- X,Y,Z Vertex 2 {m}
  88.4737163485372, 150.825304258008, 0,  !- X,Y,Z Vertex 3 {m}
  88.4737163485372, 150.825304258008, 2.8; !- X,Y,Z Vertex 4 {m}

FenestrationSurface:Detailed,
  Sub Surface B3 2,                          !- Name
  Window,                                 !- Surface Type
  Windows, !- Construction Name
  Surface B3 1,                              !- Building Surface Name
  ,                                       !- Outside Boundary Condition Object
  ,                                       !- View Factor to Ground
  ,                                       !- Shading Control Name
  ,                                       !- Frame and Divider Name
  ,                                       !- Multiplier
  ,                                       !- Number of Vertices
  99.584404879926, 139.900612534307, 1.18137151480195, !- X,Y,Z Vertex 1 {m}
  99.584404879926, 139.900612534307, 0.76, !- X,Y,Z Vertex 2 {m}
  88.4918278171483, 150.807495981709, 0.76, !- X,Y,Z Vertex 3 {m}
  88.4918278171483, 150.807495981709, 1.18137151480195; !- X,Y,Z Vertex 4 {m}

BuildingSurface:Detailed,
  Surface B3 2,                              !- Name
  Wall,                                   !- Surface Type
  Wall, !- Construction Name
  B3-1 Zone,                      !- Zone Name
  Outdoors,                               !- Outside Boundary Condition
  ,                                       !- Outside Boundary Condition Object
  SunExposed,                             !- Sun Exposure
  WindExposed,                            !- Wind Exposure
  ,                                       !- View Factor to Ground
  ,                                       !- Number of Vertices
  90.4525163485372, 130.577804258008, 2.8, !- X,Y,Z Vertex 1 {m}
  90.4525163485372, 130.577804258008, 0,  !- X,Y,Z Vertex 2 {m}
  99.6025163485372, 139.882804258008, 0,  !- X,Y,Z Vertex 3 {m}
  99.6025163485372, 139.882804258008, 2.8; !- X,Y,Z Vertex 4 {m}

FenestrationSurface:Detailed,
  Sub Surface B3 1,                          !- Name
  Window,                                 !- Surface Type
  Windows, !- Construction Name
  Surface B3 2,                              !- Building Surface Name
  ,                                       !- Outside Boundary Condition Object
  ,                                       !- View Factor to Ground
  ,                                       !- Shading Control Name
  ,                                       !- Frame and Divider Name
  ,                                       !- Multiplier
  ,                                       !- Number of Vertices
  90.4703253857737, 130.595914978389, 1.18164131714041, !- X,Y,Z Vertex 1 {m}
  90.4703253857737, 130.595914978389, 0.76, !- X,Y,Z Vertex 2 {m}
  99.5847073113008, 139.864693537627, 0.76, !- X,Y,Z Vertex 3 {m}
  99.5847073113008, 139.864693537627, 1.18164131714041; !- X,Y,Z Vertex 4 {m}

BuildingSurface:Detailed,
  Surface B3 3,                              !- Name
  Ceiling,                                !- Surface Type
  Slab Ceiling with Radiant,                       !- Construction Name
  B3-1 Zone,                      !- Zone Name
  Surface,                                !- Outside Boundary Condition
  Surface B3 46,                             !- Outside Boundary Condition Object
  NoSun,                                  !- Sun Exposure
  NoWind,                                 !- Wind Exposure
  ,                                       !- View Factor to Ground
  ,                                       !- Number of Vertices
  90.4525163485372, 130.577804258008, 2.8, !- X,Y,Z Vertex 1 {m}
  99.6025163485372, 139.882804258008, 2.8, !- X,Y,Z Vertex 2 {m}
  88.4737163485372, 150.825304258008, 2.8, !- X,Y,Z Vertex 3 {m}
  79.3237163485372, 141.520304258008, 2.8; !- X,Y,Z Vertex 4 {m}

BuildingSurface:Detailed,
  Surface B3 4,                              !- Name
  Wall,                                   !- Surface Type
  Wall, !- Construction Name
  B3-1 Zone,                      !- Zone Name
  Outdoors,                               !- Outside Boundary Condition
  ,                                       !- Outside Boundary Condition Object
  SunExposed,                             !- Sun Exposure
  WindExposed,                            !- Wind Exposure
  ,                                       !- View Factor to Ground
  ,                                       !- Number of Vertices
  88.4737163485372, 150.825304258008, 2.8, !- X,Y,Z Vertex 1 {m}
  88.4737163485372, 150.825304258008, 0,  !- X,Y,Z Vertex 2 {m}
  79.3237163485372, 141.520304258008, 0,  !- X,Y,Z Vertex 3 {m}
  79.3237163485372, 141.520304258008, 2.8; !- X,Y,Z Vertex 4 {m}

FenestrationSurface:Detailed,
  Sub Surface B3 5,                          !- Name
  Window,                                 !- Surface Type
  Windows, !- Construction Name
  Surface B3 4,                              !- Building Surface Name
  ,                                       !- Outside Boundary Condition Object
  ,                                       !- View Factor to Ground
  ,                                       !- Shading Control Name
  ,                                       !- Frame and Divider Name
  ,                                       !- Multiplier
  ,                                       !- Number of Vertices
  88.4559073113008, 150.807193537627, 1.18164131714041, !- X,Y,Z Vertex 1 {m}
  88.4559073113008, 150.807193537627, 0.76, !- X,Y,Z Vertex 2 {m}
  79.3415253857737, 141.538414978389, 0.76, !- X,Y,Z Vertex 3 {m}
  79.3415253857737, 141.538414978389, 1.18164131714041; !- X,Y,Z Vertex 4 {m}

BuildingSurface:Detailed,
  Surface B3 5,                              !- Name
  Floor,                                  !- Surface Type
  Slab Floor with Radiant,      !- Construction Name
  B3-1 Zone,                      !- Zone Name
  Ground,                                 !- Outside Boundary Condition
  ,                                       !- Outside Boundary Condition Object
  NoSun,                                  !- Sun Exposure
  NoWind,                                 !- Wind Exposure
  ,                                       !- View Factor to Ground
  ,                                       !- Number of Vertices
  88.4737163485372, 150.825304258008, 0,  !- X,Y,Z Vertex 1 {m}
  99.6025163485372, 139.882804258008, 0,  !- X,Y,Z Vertex 2 {m}
  90.4525163485372, 130.577804258008, 0,  !- X,Y,Z Vertex 3 {m}
  79.3237163485372, 141.520304258008, 0;  !- X,Y,Z Vertex 4 {m}

BuildingSurface:Detailed,
  Surface B3 6,                              !- Name
  Wall,                                   !- Surface Type
  Wall, !- Construction Name
  B3-1 Zone,                      !- Zone Name
  Outdoors,                               !- Outside Boundary Condition
  ,                                       !- Outside Boundary Condition Object
  SunExposed,                             !- Sun Exposure
  WindExposed,                            !- Wind Exposure
  ,                                       !- View Factor to Ground
  ,                                       !- Number of Vertices
  79.3237163485372, 141.520304258008, 2.8, !- X,Y,Z Vertex 1 {m}
  79.3237163485372, 141.520304258008, 0,  !- X,Y,Z Vertex 2 {m}
  90.4525163485372, 130.577804258008, 0,  !- X,Y,Z Vertex 3 {m}
  90.4525163485372, 130.577804258008, 2.8; !- X,Y,Z Vertex 4 {m}

FenestrationSurface:Detailed,
  Sub Surface B3 3,                          !- Name
  Window,                                 !- Surface Type
  Windows, !- Construction Name
  Surface B3 6,                              !- Building Surface Name
  ,                                       !- Outside Boundary Condition Object
  ,                                       !- View Factor to Ground
  ,                                       !- Shading Control Name
  ,                                       !- Frame and Divider Name
  ,                                       !- Multiplier
  ,                                       !- Number of Vertices
  79.3418278171483, 141.502495981709, 1.18137151480195, !- X,Y,Z Vertex 1 {m}
  79.3418278171483, 141.502495981709, 0.76, !- X,Y,Z Vertex 2 {m}
  90.434404879926, 130.595612534307, 0.76, !- X,Y,Z Vertex 3 {m}
  90.434404879926, 130.595612534307, 1.18137151480195; !- X,Y,Z Vertex 4 {m}

Zone,
  B3-2 Zone,                      !- Name
  -0,                                     !- Direction of Relative North {deg}
  0,                                      !- X Origin {m}
  0,                                      !- Y Origin {m}
  2.8,                                    !- Z Origin {m}
  ,                                       !- Type
  ,                                       !- Multiplier
  ,                                       !- Ceiling Height {m}
  ,                                       !- Volume {m3}
  ,                                       !- Floor Area {m2}
  ,                                       !- Zone Inside Convection Algorithm
  ;                                       !- Zone Outside Convection Algorithm

BuildingSurface:Detailed,
  Surface B3 43,                             !- Name
  Wall,                                   !- Surface Type
  Wall, !- Construction Name
  B3-2 Zone,                      !- Zone Name
  Outdoors,                               !- Outside Boundary Condition
  ,                                       !- Outside Boundary Condition Object
  SunExposed,                             !- Sun Exposure
  WindExposed,                            !- Wind Exposure
  ,                                       !- View Factor to Ground
  ,                                       !- Number of Vertices
  99.6025163485372, 139.882804258008, 2.8, !- X,Y,Z Vertex 1 {m}
  99.6025163485372, 139.882804258008, 0,  !- X,Y,Z Vertex 2 {m}
  88.4737163485372, 150.825304258008, 0,  !- X,Y,Z Vertex 3 {m}
  88.4737163485372, 150.825304258008, 2.8; !- X,Y,Z Vertex 4 {m}

FenestrationSurface:Detailed,
  Sub Surface B3 7,                          !- Name
  Window,                                 !- Surface Type
  Windows, !- Construction Name
  Surface B3 43,                             !- Building Surface Name
  ,                                       !- Outside Boundary Condition Object
  ,                                       !- View Factor to Ground
  ,                                       !- Shading Control Name
  ,                                       !- Frame and Divider Name
  ,                                       !- Multiplier
  ,                                       !- Number of Vertices
  99.584404879926, 139.900612534307, 1.18137151480195, !- X,Y,Z Vertex 1 {m}
  99.584404879926, 139.900612534307, 0.76, !- X,Y,Z Vertex 2 {m}
  88.4918278171483, 150.807495981709, 0.76, !- X,Y,Z Vertex 3 {m}
  88.4918278171483, 150.807495981709, 1.18137151480195; !- X,Y,Z Vertex 4 {m}

BuildingSurface:Detailed,
  Surface B3 44,                             !- Name
  Wall,                                   !- Surface Type
  Wall, !- Construction Name
  B3-2 Zone,                      !- Zone Name
  Outdoors,                               !- Outside Boundary Condition
  ,                                       !- Outside Boundary Condition Object
  SunExposed,                             !- Sun Exposure
  WindExposed,                            !- Wind Exposure
  ,                                       !- View Factor to Ground
  ,                                       !- Number of Vertices
  90.4525163485372, 130.577804258008, 2.8, !- X,Y,Z Vertex 1 {m}
  90.4525163485372, 130.577804258008, 0,  !- X,Y,Z Vertex 2 {m}
  99.6025163485372, 139.882804258008, 0,  !- X,Y,Z Vertex 3 {m}
  99.6025163485372, 139.882804258008, 2.8; !- X,Y,Z Vertex 4 {m}

FenestrationSurface:Detailed,
  Sub Surface B3 8,                          !- Name
  Window,                                 !- Surface Type
  Windows, !- Construction Name
  Surface B3 44,                             !- Building Surface Name
  ,                                       !- Outside Boundary Condition Object
  ,                                       !- View Factor to Ground
  ,                                       !- Shading Control Name
  ,                                       !- Frame and Divider Name
  ,                                       !- Multiplier
  ,                                       !- Number of Vertices
  90.4703253857737, 130.595914978389, 1.18164131714041, !- X,Y,Z Vertex 1 {m}
  90.4703253857737, 130.595914978389, 0.76, !- X,Y,Z Vertex 2 {m}
  99.5847073113008, 139.864693537627, 0.76, !- X,Y,Z Vertex 3 {m}
  99.5847073113008, 139.864693537627, 1.18164131714041; !- X,Y,Z Vertex 4 {m}

BuildingSurface:Detailed,
  Surface B3 45,                             !- Name
  Wall,                                   !- Surface Type
  Wall, !- Construction Name
  B3-2 Zone,                      !- Zone Name
  Outdoors,                               !- Outside Boundary Condition
  ,                                       !- Outside Boundary Condition Object
  SunExposed,                             !- Sun Exposure
  WindExposed,                            !- Wind Exposure
  ,                                       !- View Factor to Ground
  ,                                       !- Number of Vertices
  88.4737163485372, 150.825304258008, 2.8, !- X,Y,Z Vertex 1 {m}
  88.4737163485372, 150.825304258008, 0,  !- X,Y,Z Vertex 2 {m}
  79.3237163485372, 141.520304258008, 0,  !- X,Y,Z Vertex 3 {m}
  79.3237163485372, 141.520304258008, 2.8; !- X,Y,Z Vertex 4 {m}

FenestrationSurface:Detailed,
  Sub Surface B3 6,                          !- Name
  Window,                                 !- Surface Type
  Windows, !- Construction Name
  Surface B3 45,                             !- Building Surface Name
  ,                                       !- Outside Boundary Condition Object
  ,                                       !- View Factor to Ground
  ,                                       !- Shading Control Name
  ,                                       !- Frame and Divider Name
  ,                                       !- Multiplier
  ,                                       !- Number of Vertices
  88.4559073113008, 150.807193537627, 1.18164131714041, !- X,Y,Z Vertex 1 {m}
  88.4559073113008, 150.807193537627, 0.76, !- X,Y,Z Vertex 2 {m}
  79.3415253857737, 141.538414978389, 0.76, !- X,Y,Z Vertex 3 {m}
  79.3415253857737, 141.538414978389, 1.18164131714041; !- X,Y,Z Vertex 4 {m}

BuildingSurface:Detailed,
  Surface B3 46,                             !- Name
  Floor,                                  !- Surface Type
  Slab Floor with Radiant,                         !- Construction Name
  B3-2 Zone,                      !- Zone Name
  Surface,                                !- Outside Boundary Condition
  Surface B3 3,                              !- Outside Boundary Condition Object
  NoSun,                                  !- Sun Exposure
  NoWind,                                 !- Wind Exposure
  ,                                       !- View Factor to Ground
  ,                                       !- Number of Vertices
  88.4737163485372, 150.825304258008, 0,  !- X,Y,Z Vertex 1 {m}
  99.6025163485372, 139.882804258008, 0,  !- X,Y,Z Vertex 2 {m}
  90.4525163485372, 130.577804258008, 0,  !- X,Y,Z Vertex 3 {m}
  79.3237163485372, 141.520304258008, 0;  !- X,Y,Z Vertex 4 {m}

BuildingSurface:Detailed,
  Surface B3 47,                             !- Name
  Wall,                                   !- Surface Type
  Wall, !- Construction Name
  B3-2 Zone,                      !- Zone Name
  Outdoors,                               !- Outside Boundary Condition
  ,                                       !- Outside Boundary Condition Object
  SunExposed,                             !- Sun Exposure
  WindExposed,                            !- Wind Exposure
  ,                                       !- View Factor to Ground
  ,                                       !- Number of Vertices
  79.3237163485372, 141.520304258008, 2.8, !- X,Y,Z Vertex 1 {m}
  79.3237163485372, 141.520304258008, 0,  !- X,Y,Z Vertex 2 {m}
  90.4525163485372, 130.577804258008, 0,  !- X,Y,Z Vertex 3 {m}
  90.4525163485372, 130.577804258008, 2.8; !- X,Y,Z Vertex 4 {m}

FenestrationSurface:Detailed,
  Sub Surface B3 4,                          !- Name
  Window,                                 !- Surface Type
  Windows, !- Construction Name
  Surface B3 47,                             !- Building Surface Name
  ,                                       !- Outside Boundary Condition Object
  ,                                       !- View Factor to Ground
  ,                                       !- Shading Control Name
  ,                                       !- Frame and Divider Name
  ,                                       !- Multiplier
  ,                                       !- Number of Vertices
  79.3418278171483, 141.502495981709, 1.18137151480195, !- X,Y,Z Vertex 1 {m}
  79.3418278171483, 141.502495981709, 0.76, !- X,Y,Z Vertex 2 {m}
  90.434404879926, 130.595612534307, 0.76, !- X,Y,Z Vertex 3 {m}
  90.434404879926, 130.595612534307, 1.18137151480195; !- X,Y,Z Vertex 4 {m}

BuildingSurface:Detailed,
  Surface B3 48,                             !- Name
  Roof,                                   !- Surface Type
  Roof,      !- Construction Name
  B3-2 Zone,                      !- Zone Name
  Ground,                                 !- Outside Boundary Condition
  ,                                       !- Outside Boundary Condition Object
  NoSun,                                  !- Sun Exposure
  NoWind,                                 !- Wind Exposure
  ,                                       !- View Factor to Ground
  ,                                       !- Number of Vertices
  90.4525163485372, 130.577804258008, 2.8, !- X,Y,Z Vertex 1 {m}
  99.6025163485372, 139.882804258008, 2.8, !- X,Y,Z Vertex 2 {m}
  88.4737163485372, 150.825304258008, 2.8, !- X,Y,Z Vertex 3 {m}
  79.3237163485372, 141.520304258008, 2.8; !- X,Y,Z Vertex 4 {m}

!==================================== End Building 3 Surface=====================================
##fileprefix C:\EnergyPlusV8-1-0\ExampleFiles\Configurations\
##include ConstructionandMaterials.idf
!====================================End Radiant Branch========================================================