Place the "ExampleFiles folder" folder to the following directory: "C:\EnergyPlusV8-1-0\ExampleFiles\Configurations\"

1.Loops supplying SH Demand
  Decentralized production+ Centralized storage 
  Collector output variables: D_CL_collectors_all_smallpipetest.imf
  Pipe output variables: D_CL_Pipes_smallpipetest.imf
  Pump output variables: D_CL_Pumps_smallpipetest.imf  
  Water heater output variables: D_CL_waterheater_smallpipetest.imf  

2.Loops supplying SH Demand
  Centralized production+ Centralized storage
  Collector output variables: C_CL_centralcollectors_smallpipetest_10m.imf
  Pipe output variables: C_CL_pipes_smallpipetest_10m.imf
  Pump output variables: C_CL_pumps_smallpipetest_10m.imf  
  Water heater output variables: C_CL_waterheater_smallpipetest_10m.imf

3.Loops supplying SH Demand
  Decentralized production+ decentralized storage
  DS.imf
  DS_2.imf