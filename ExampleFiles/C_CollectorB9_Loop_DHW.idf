!***PLANT Collector B9 DHW Loop***
Sizing:Plant,
    Collector B9 DHW Loop,       !- Plant or Condenser Loop Name
    HEATING,                 !- Loop Type
    82.,                     !- Design Loop Exit Temperature {C}
    11;                      !- Loop Design Temperature Difference {deltaC}

PlantLoop,
    Collector B9 DHW Loop,       !- Name
    WATER,                   !- Fluid Type
    ,                        !- User Defined Fluid Type
    Collector B9 DHW Loop Operation,  !- Plant Equipment Operation Scheme Name
    B9 DHW Tank Source Loop Outlet Node,  !- Loop Temperature Setpoint Node Name
    100,                     !- Maximum Loop Temperature {C}
    3,                       !- Minimum Loop Temperature {C}
    AUTOSIZE,                !- Maximum Loop Flow Rate {m3/s}
    0.0,                     !- Minimum Loop Flow Rate {m3/s}
    autocalculate,           !- Plant Loop Volume {m3}
    B9 DHW Tank Source Loop Inlet Node,  !- Plant Side Inlet Node Name
    B9 DHW Tank Source Loop Outlet Node,  !- Plant Side Outlet Node Name
    B9 DHW Tank Source Branches,  !- Plant Side Branch List Name
    B9 DHW Tank Source Connectors,  !- Plant Side Connector List Name
    Collector B9 DHW Loop Inlet Node,  !- Demand Side Inlet Node Name
    Collector B9 DHW Loop Outlet Node,  !- Demand Side Outlet Node Name
    Collector B9 DHW Loop Branches,   !- Demand Side Branch List Name
    Collector B9 DHW Loop Connectors, !- Demand Side Connector List Name
    OPTIMAL,                 !- Load Distribution Scheme
    Collector B9 DHW Loop Availability Manager List;  !- Availability Manager List Name

PlantEquipmentOperationSchemes,
    Collector B9 DHW Loop Operation,  !- Name
    PlantEquipmentOperation:HeatingLoad,  !- Control Scheme 1 Object Type
    Collector B9 DHW Loop Control Scheme,  !- Control Scheme 1 Name
    AlwaysOnSchedule;        !- Control Scheme 1 Schedule Name

PlantEquipmentOperation:HeatingLoad,
    Collector B9 DHW Loop Control Scheme,  !- Name
    0,                       !- Load Range 1 Lower Limit {W}
    10000000,                !- Load Range 1 Upper Limit {W}
    Collector B9 DHW Loop Plant Equipment;  !- Range 1 Equipment List Name

PlantEquipmentList,
    Collector B9 DHW Loop Plant Equipment,  !- Name
    WaterHeater:Stratified,  !- Equipment 1 Object Type
    B9 DHW Tank;             !- Equipment 1 Name

SetpointManager:Scheduled,
    Collector B9 DHW Loop Setpoint Manager,  !- Name
    Temperature,             !- Control Variable
    Collector Loop Temperature Schedule,  !- Schedule Name
    B9 DHW Tank Source Loop Outlet Node;  !- Setpoint Node or NodeList Name

!Schedule:Compact,
 !   Collector Loop Temperature Schedule,  !- Name
 !   Any Number,              !- Schedule Type Limits Name
 !   THROUGH: 12/31,          !- Field 1
 !   FOR: AllDays,            !- Field 2
 !   UNTIL: 24:00, 60;        !- Field 4

AvailabilityManagerAssignmentList,
    Collector B9 DHW Loop Availability Manager List,  !- Name
    AvailabilityManager:HighTemperatureTurnOff,  !- Availability Manager 1 Object Type
    B9 DHW Loop High Temperature Turn Off Availability Manager,  !- Availability Manager 1 Name
    AvailabilityManager:LowTemperatureTurnOn,  !- Availability Manager 2 Object Type
    B9 DHW Loop Low Temperature Turn On Availability Manager,  !- Availability Manager 2 Name
    AvailabilityManager:DifferentialThermostat,  !- Availability Manager 3 Object Type
    B9 DHW Loop Differential Thermostat Availability Manager;  !- Availability Manager 3 Name

AvailabilityManager:HighTemperatureTurnOff,
    B9 DHW Loop High Temperature Turn Off Availability Manager,  !- Name
    B9 DHW Tank Use Outlet Node,  !- Sensor Node Name
    95;                      !- Temperature {C}

AvailabilityManager:LowTemperatureTurnOn,
    B9 DHW Loop Low Temperature Turn On Availability Manager,  !- Name
    Collector B9 26 Outlet Node,  !- Sensor Node Name
    -10.0;                     !- Temperature {C}

AvailabilityManager:DifferentialThermostat,
    B9 DHW Loop Differential Thermostat Availability Manager,  !- Name
    Collector B9 26 Outlet Node,  !- Hot Node Name
    B9 DHW Tank Source Outlet Node,  !- Cold Node Name
    10.0,                    !- Temperature Difference On Limit {deltaC}
    2.0;                     !- Temperature Difference Off Limit {deltaC}

!***PLANT Collector B9 DHW Loop: Demand***
BranchList,
    B9 DHW Tank Source Branches,  !- Name
    B9 DHW Tank Source Inlet Branch,  !- Branch 1 Name
    B9 DHW Tank Source Branch,  !- Branch 2 Name
    B9 DHW Tank Source Outlet Branch;  !- Branch 3 Name

ConnectorList,
    B9 DHW Tank Source Connectors,  !- Name
    Connector:Splitter,      !- Connector 1 Object Type
    B9 DHW Tank Source Splitter,  !- Connector 1 Name
    Connector:Mixer,         !- Connector 2 Object Type
    B9 DHW Tank Source Mixer;!- Connector 2 Name

Connector:Splitter,
    B9 DHW Tank Source Splitter,  !- Name
    B9 DHW Tank Source Inlet Branch,  !- Inlet Branch Name
    B9 DHW Tank Source Branch;  !- Outlet Branch 1 Name

Connector:Mixer,
    B9 DHW Tank Source Mixer,!- Name
    B9 DHW Tank Source Outlet Branch,  !- Outlet Branch Name
    B9 DHW Tank Source Branch;  !- Inlet Branch 1 Name

Branch,
    B9 DHW Tank Source Inlet Branch,  !- Name
    ,                       !- Maximum Flow Rate {m3/s}
    ,                        !- Pressure Drop Curve Name
    Pump:VariableSpeed,      !- Component 1 Object Type
    Collector B9 DHW Loop Pump,  !- Component 1 Name
    B9 DHW Tank Source Loop Inlet Node,  !- Component 1 Inlet Node Name
    B9 DHW Tank Source Pump-Water Heater Node,  !- Component 1 Outlet Node Name
    ACTIVE;                  !- Component 1 Branch Control Type

Pump:VariableSpeed,
    Collector B9 DHW Loop Pump,  !- Name
    B9 DHW Tank Source Loop Inlet Node,  !- Inlet Node Name
    B9 DHW Tank Source Pump-Water Heater Node,  !- Outlet Node Name
    AUTOSIZE,                !- Rated Flow Rate {m3/s}
    120000,                  !- Rated Pump Head {Pa}
    AUTOSIZE,                !- Rated Power Consumption {W}
    0.87,                    !- Motor Efficiency
    0.0,                     !- Fraction of Motor Inefficiencies to Fluid Stream
    0,                       !- Coefficient 1 of the Part Load Performance Curve
    1,                       !- Coefficient 2 of the Part Load Performance Curve
    0,                       !- Coefficient 3 of the Part Load Performance Curve
    0,                       !- Coefficient 4 of the Part Load Performance Curve
    0.0,                     !- Minimum Flow Rate {m3/s}
    INTERMITTENT;            !- Pump Control Type

Branch,
    B9 DHW Tank Source Branch,  !- Name
    ,                       !- Maximum Flow Rate {m3/s}
    ,                        !- Pressure Drop Curve Name
    WaterHeater:Stratified,  !- Component 1 Object Type
    B9 DHW Tank,             !- Component 1 Name
    B9 DHW Tank Source Inlet Node,  !- Component 1 Inlet Node Name
    B9 DHW Tank Source Outlet Node,  !- Component 1 Outlet Node Name
    PASSIVE;                 !- Component 1 Branch Control Type

WaterHeater:Stratified,
    B9 DHW Tank,             !- Name
    General,                 !- End-Use Subcategory
    1.8,                       !- Tank Volume {m3}
    2.78,                       !- Tank Height {m}
    VerticalCylinder,        !- Tank Shape
    ,                        !- Tank Perimeter {m}
    100,                     !- Maximum Temperature Limit {C}
    MasterSlave,             !- Heater Priority Control
    B9 DHW Tank Setpoint Temp Schedule,  !- Heater 1 Setpoint Temperature Schedule Name
    15,                      !- Heater 1 Deadband Temperature Difference {deltaC}
    ,                        !- Heater 1 Capacity {W}
    ,                        !- Heater 1 Height {m}
    B9 DHW Tank Setpoint Temp Schedule,  !- Heater 2 Setpoint Temperature Schedule Name
    ,                        !- Heater 2 Deadband Temperature Difference {deltaC}
    ,                        !- Heater 2 Capacity {W}
    ,                        !- Heater 2 Height {m}
    Electricity,             !- Heater Fuel Type
    0.98,                    !- Heater Thermal Efficiency
    ,                        !- Off Cycle Parasitic Fuel Consumption Rate {W}
    ,                        !- Off Cycle Parasitic Fuel Type
    ,                        !- Off Cycle Parasitic Heat Fraction to Tank
    ,                        !- Off Cycle Parasitic Height {m}
    ,                        !- On Cycle Parasitic Fuel Consumption Rate {W}
    ,                        !- On Cycle Parasitic Fuel Type
    ,                        !- On Cycle Parasitic Heat Fraction to Tank
    ,                        !- On Cycle Parasitic Height {m}
    Outdoors,                !- Ambient Temperature Indicator
    ,   !- Ambient Temperature Schedule Name
    ,                        !- Ambient Temperature Zone Name
    Outdoor Air Node 9,                        !- Ambient Temperature Outdoor Air Node Name
    0.225,                   !- Uniform Skin Loss Coefficient per Unit Area to Ambient Temperature {W/m2-K}
    1,                       !- Skin Loss Fraction to Zone
    ,                        !- Off Cycle Flue Loss Coefficient to Ambient Temperature {W/K}
    1,                       !- Off Cycle Flue Loss Fraction to Zone
    ,                        !- Peak Use Flow Rate {m3/s}
    ,                        !- Use Flow Rate Fraction Schedule Name
    ,                        !- Cold Water Supply Temperature Schedule Name
    B9 DHW Tank Use Inlet Node,  !- Use Side Inlet Node Name
    B9 DHW Tank Use Outlet Node,  !- Use Side Outlet Node Name
    1,                       !- Use Side Effectiveness
    ,                        !- Use Side Inlet Height {m}
    autocalculate,           !- Use Side Outlet Height {m}
    B9 DHW Tank Source Inlet Node,  !- Source Side Inlet Node Name
    B9 DHW Tank Source Outlet Node,  !- Source Side Outlet Node Name
    1,                       !- Source Side Effectiveness
    autocalculate,           !- Source Side Inlet Height {m}
    ,                        !- Source Side Outlet Height {m}
    Seeking,                 !- Inlet Mode
    autosize,                !- Use Side Design Flow Rate {m3/s}
    autosize,                !- Source Side Design Flow Rate {m3/s}
    1.5,                     !- Indirect Water Heating Recovery Time {hr}
    3,                       !- Number of Nodes
    0.1,                     !- Additional Destratification Conductivity {W/m-K}
    0.15,                    !- Node 1 Additional Loss Coefficient {W/m2-K}
    ,                     !- Node 2 Additional Loss Coefficient {W/m2-K}
    0.1,                        !- Node 3 Additional Loss Coefficient {W/m2-K}
    ,                        !- Node 4 Additional Loss Coefficient {W/m2-K}
    ,                        !- Node 5 Additional Loss Coefficient {W/m2-K}
    ,                        !- Node 6 Additional Loss Coefficient {W/m2-K}
    ,                        !- Node 7 Additional Loss Coefficient {W/m2-K}
    ,                        !- Node 8 Additional Loss Coefficient {W/m2-K}
    ,                        !- Node 9 Additional Loss Coefficient {W/m2-K}
    ,                        !- Node 10 Additional Loss Coefficient {W/m2-K}
    IndirectHeatPrimarySetpoint;  !- Source Side Flow Control Mode

OutdoorAir:Node,
    Outdoor Air Node 9,        !- Name
    3;                       !- Height Above Ground {m}

Schedule:Compact,
    B9 DHW Tank Setpoint Temp Schedule,  !- Name
    Any Number,              !- Schedule Type Limits Name
    Through: 5/15,           !- Field 1
    For: Alldays,            !- Field 2
    Until: 24:00, 50,      !- Field 4
    Through: 9/15,           !- Field 5
    For: Alldays,            !- Field 6
    Until: 24:00, 80,         !- Field 8
    Through: 12/31,          !- Field 9
    For: Alldays,            !- Field 10
    Until: 24:00, 50;         !- Field 12

Branch,
    B9 DHW Tank Source Outlet Branch,  !- Name
    ,                       !- Maximum Flow Rate {m3/s}
    ,                        !- Pressure Drop Curve Name
    Pipe:Adiabatic,          !- Component 1 Object Type
    B9 DHW Tank Source Outlet Pipe,  !- Component 1 Name
    B9 DHW Tank Source Water Heater-Pipe Node,  !- Component 1 Inlet Node Name
    B9 DHW Tank Source Loop Outlet Node,  !- Component 1 Outlet Node Name
    PASSIVE;                 !- Component 1 Branch Control Type

Pipe:Adiabatic,
    B9 DHW Tank Source Outlet Pipe,  !- Name
    B9 DHW Tank Source Water Heater-Pipe Node,  !- Inlet Node Name
    B9 DHW Tank Source Loop Outlet Node;  !- Outlet Node Name

!==========Collector Loop: Collector Side=========
##fileprefix C:\EnergyPlusV8-1-0\ExampleFiles\Configurations\
##include Collectors_B9_DHW.idf
!=================================================================