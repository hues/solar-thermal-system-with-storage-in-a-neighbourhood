!==============================================================================================================
!***PLANT B11 DHW LOOP***
Sizing:Plant,
    B11 DHW Loop,                !- Plant or Condenser Loop Name
    HEATING,                 !- Loop Type
    82.,                     !- Design Loop Exit Temperature {C}
    11;                      !- Loop Design Temperature Difference {deltaC}

PlantLoop,
    B11 DHW Loop,                !- Name
    WATER,                   !- Fluid Type
    ,                        !- User Defined Fluid Type
    B11 DHW Loop Operation Scheme,  !- Plant Equipment Operation Scheme Name
    B11 DHW Heater Outlet Node,  !- Loop Temperature Setpoint Node Name
    100,                     !- Maximum Loop Temperature {C}
    3,                       !- Minimum Loop Temperature {C}
    AUTOSIZE,                !- Maximum Loop Flow Rate {m3/s}
    0,                       !- Minimum Loop Flow Rate {m3/s}
    autocalculate,           !- Plant Loop Volume {m3}
    B11 DHW Tank Use Loop Inlet Node,  !- Plant Side Inlet Node Name
    B11 DHW Heater Outlet Node,  !- Plant Side Outlet Node Name
    B11 DHW Tank Use Branches,  !- Plant Side Branch List Name
    B11 DHW Tank Use Connectors,  !- Plant Side Connector List Name
    B11 DHW Demand Inlet Node,   !- Demand Side Inlet Node Name
    B11 DHW Demand Outlet Node,  !- Demand Side Outlet Node Name
    B11 DHW Demand Branches,     !- Demand Side Branch List Name
    B11 DHW Demand Connectors,   !- Demand Side Connector List Name
    OPTIMAL;                 !- Load Distribution Scheme

PlantEquipmentOperationSchemes,
    B11 DHW Loop Operation Scheme,  !- Name
    PlantEquipmentOperation:HeatingLoad,  !- Control Scheme 1 Object Type
    B11 DHW Loop Operation Scheme,  !- Control Scheme 1 Name
    AlwaysOnSchedule;        !- Control Scheme 1 Schedule Name

PlantEquipmentOperation:HeatingLoad,
    B11 DHW Loop Operation Scheme,  !- Name
    0,                       !- Load Range 1 Lower Limit {W}
    10000000,                !- Load Range 1 Upper Limit {W}
    B11 DHW Loop Equipment List; !- Range 1 Equipment List Name

PlantEquipmentList,
    B11 DHW Loop Equipment List, !- Name
    WaterHeater:Stratified,  !- Equipment 1 Object Type
    B11 DHW Tank,  !- Equipment 1 Name
    TemperingValve,          !- Equipment 2 Object Type
    B11 DHW Anti-Scald Diverter, !- Equipment 2 Name
    WaterHeater:Mixed,       !- Equipment 3 Object Type
    B11 DHW Heater;              !- Equipment 3 Name

SetpointManager:Scheduled,
    B11 DHW Loop Setpoint Manager,  !- Name
    Temperature,             !- Control Variable
    B11 DHW Loop Temperature Schedule,  !- Schedule Name
    B11 DHW Loop Setpoint NodeList;  !- Setpoint Node or NodeList Name
NodeList,
    B11 DHW Loop Setpoint NodeList,  !- Name
    B11 DHW Heater Inlet Node,  !- Node 1 Name
    B11 DHW Heater Outlet Node;  !- Node 2 Name

Schedule:Compact,
    B11 DHW Loop Temperature Schedule,  !- Name
    Any Number,              !- Schedule Type Limits Name
    THROUGH: 12/31,          !- Field 1
    FOR: AllDays,            !- Field 2
    UNTIL: 24:00, 45;        !- Field 4

!***PLANT B11 DHW LOOP: B11 DHW SIDE***
BranchList,
    B11 DHW Demand Branches,     !- Name
    B11 DHW Demand Inlet Branch, !- Branch 1 Name
    B11 DHW Branch,  !- Branch 2 Name
    B11 DHW Demand Outlet Branch;!- Branch 3 Name

ConnectorList,
    B11 DHW Demand Connectors,   !- Name
    Connector:Splitter,      !- Connector 1 Object Type
    B11 DHW Demand Splitter,     !- Connector 1 Name
    Connector:Mixer,         !- Connector 2 Object Type
    B11 DHW Demand Mixer;        !- Connector 2 Name

Connector:Splitter,
    B11 DHW Demand Splitter,     !- Name
    B11 DHW Demand Inlet Branch, !- Inlet Branch Name
    B11 DHW Branch;  !- Outlet Branch 1 Name

Connector:Mixer,
    B11 DHW Demand Mixer,        !- Name
    B11 DHW Demand Outlet Branch,!- Outlet Branch Name
    B11 DHW Branch;  !- Inlet Branch 1 Name

Branch,
    B11 DHW Demand Inlet Branch, !- Name
    ,                        !- Maximum Flow Rate {m3/s}
    ,                        !- Pressure Drop Curve Name
    Pipe:Adiabatic,          !- Component 1 Object Type
    B11 DHW Demand Inlet Pipe,   !- Component 1 Name
    B11 DHW Demand Inlet Node,   !- Component 1 Inlet Node Name
    B11 DHW Demand Pipe-DHW Node,!- Component 1 Outlet Node Name
    PASSIVE;                 !- Component 1 Branch Control Type

Pipe:Adiabatic,
    B11 DHW Demand Inlet Pipe,   !- Name
    B11 DHW Demand Inlet Node,   !- Inlet Node Name
    B11 DHW Demand Pipe-DHW Node;!- Outlet Node Name

Branch,
    B11 DHW Demand OUTLET Branch,!- Name
    ,                        !- Maximum Flow Rate {m3/s}
    ,                        !- Pressure Drop Curve Name
    Pipe:Adiabatic,          !- Component 1 Object Type
    B11 DHW Demand OUTLET Pipe,  !- Component 1 Name
    B11 DHW Demand DHW -Pipe Node,!- Component 1 Inlet Node Name
    B11 DHW Demand Outlet Node,  !- Component 1 Outlet Node Name
    PASSIVE;                 !- Component 1 Branch Control Type

Pipe:Adiabatic,
    B11 DHW Demand OUTLET Pipe,  !- Name
    B11 DHW Demand DHW -Pipe Node,!- Inlet Node Name
    B11 DHW Demand Outlet Node;  !- Outlet Node Name

Branch,
    B11 DHW Branch,  !- Name
    0,                       !- Maximum Flow Rate {m3/s}
    ,                        !- Pressure Drop Curve Name
    WaterUse:Connections,    !- Component 1 Object Type
    B11 DHW,      !- Component 1 Name
    B11 DHW Inlet Node,  !- Component 1 Inlet Node Name
    B11 DHW Outlet Node,  !- Component 1 Outlet Node Name
    ACTIVE;                  !- Component 1 Branch Control Type

WaterUse:Connections,
    B11 DHW,      !- Name
    B11 DHW Inlet Node,  !- Inlet Node Name
    B11 DHW Outlet Node,  !- Outlet Node Name
    ,                        !- Supply Water Storage Tank Name
    ,                        !- Reclamation Water Storage Tank Name
    ,                        !- Hot Water Supply Temperature Schedule Name
    ,                        !- Cold Water Supply Temperature Schedule Name
    NONE,                    !- Drain Water Heat Exchanger Type
    ,                        !- Drain Water Heat Exchanger Destination
    ,                        !- Drain Water Heat Exchanger U-Factor Times Area {W/K}
    B11 DHW;                   !- Water Use Equipment 1 Name


WaterUse:Equipment,
    B11 DHW ,                     !- Name
    B11 DHW ,                     !- End-Use Subcategory
    0.0000256,               !- Peak Flow Rate {m3/s}
    DHW Flow Schedule,       !- Flow Rate Fraction Schedule Name
    B11 DHW Temp Schedule;       !- Target Temperature Schedule Name

Schedule:Compact,
    B11 DHW Temp Schedule,       !- Name
    Any Number,              !- Schedule Type Limits Name
    THROUGH: 12/31,          !- Field 1
    FOR: AllDays,            !- Field 2
    UNTIL: 24:00, 45;        !- Field 4
    

    
    
!***PLANT B11 DHW LOOP: WATER HEATER SIDE***
BranchList,
    B11 DHW Tank Use Branches,  !- Name
    B11 DHW Tank Use Inlet Branch,  !- Branch 1 Name
    B11 DHW Tank Use Branch,  !- Branch 2 Name
    B11 DHW Loop Tempering Branch,  !- Branch 3 Name
    B11 DHW Tank Use Outlet Branch;  !- Branch 4 Name

ConnectorList,
    B11 DHW Tank Use Connectors,  !- Name
    Connector:Splitter,      !- Connector 1 Object Type
    B11 DHW Tank Use Splitter,  !- Connector 1 Name
    Connector:Mixer,         !- Connector 2 Object Type
    B11 DHW Tank Use Mixer;  !- Connector 2 Name

Connector:Splitter,
    B11 DHW Tank Use Splitter,  !- Name
    B11 DHW Tank Use Inlet Branch,  !- Inlet Branch Name
    B11 DHW Tank Use Branch,  !- Outlet Branch 1 Name
    B11 DHW Loop Tempering Branch;  !- Outlet Branch 2 Name

Connector:Mixer,
    B11 DHW Tank Use Mixer,  !- Name
    B11 DHW Tank Use Outlet Branch,  !- Outlet Branch Name
    B11 DHW Tank Use Branch,  !- Inlet Branch 1 Name
    B11 DHW Loop Tempering Branch;  !- Inlet Branch 2 Name

Branch,
    B11 DHW Tank Use Inlet Branch,  !- Name
    0,                       !- Maximum Flow Rate {m3/s}
    ,                        !- Pressure Drop Curve Name
    Pump:VariableSpeed,      !- Component 1 Object Type
    B11 DHW Loop Pump,           !- Component 1 Name
    B11 DHW Tank Use Loop Inlet Node,  !- Component 1 Inlet Node Name
    B11 DHW Tank Use Side Pump-Tank Node,  !- Component 1 Outlet Node Name
    passive;                 !- Component 1 Branch Control Type

Pump:VariableSpeed,
    B11 DHW Loop Pump,           !- Name
    B11 DHW Tank Use Loop Inlet Node,  !- Inlet Node Name
    B11 DHW Tank Use Side Pump-Tank Node,  !- Outlet Node Name
    AUTOSIZE,                !- Rated Flow Rate {m3/s}
    100000,                  !- Rated Pump Head {Pa}
    AUTOSIZE,                !- Rated Power Consumption {W}
    0.87,                    !- Motor Efficiency
    0.0,                     !- Fraction of Motor Inefficiencies to Fluid Stream
    0,                       !- Coefficient 1 of the Part Load Performance Curve
    1,                       !- Coefficient 2 of the Part Load Performance Curve
    0,                       !- Coefficient 3 of the Part Load Performance Curve
    0,                       !- Coefficient 4 of the Part Load Performance Curve
    0,                       !- Minimum Flow Rate {m3/s}
    INTERMITTENT;            !- Pump Control Type

Branch,
    B11 DHW Tank Use Branch,  !- Name
    0,                       !- Maximum Flow Rate {m3/s}
    ,                        !- Pressure Drop Curve Name
    WaterHeater:Stratified,  !- Component 1 Object Type
    B11 DHW Tank,  !- Component 1 Name
    B11 DHW Tank Use Inlet Node,  !- Component 1 Inlet Node Name
    B11 DHW Tank Use Outlet Node,  !- Component 1 Outlet Node Name
    PASSIVE;                 !- Component 1 Branch Control Type

Branch,
    B11 DHW Loop Tempering Branch,  !- Name
    0,                       !- Maximum Flow Rate {m3/s}
    ,                        !- Pressure Drop Curve Name
    TemperingValve,          !- Component 1 Object Type
    B11 DHW Anti-Scald Diverter, !- Component 1 Name
    B11 DHW Anti-Scald Inlet Node,  !- Component 1 Inlet Node Name
    B11 DHW Anti-Scald Outlet Node,  !- Component 1 Outlet Node Name
    ACTIVE;                  !- Component 1 Branch Control Type

TemperingValve,
    B11 DHW Anti-Scald Diverter, !- Name
    B11 DHW Anti-Scald Inlet Node,  !- Inlet Node Name
    B11 DHW Anti-Scald Outlet Node,  !- Outlet Node Name
    B11 DHW Tank Use Outlet Node,  !- Stream 2 Source Node Name
    B11 DHW Heater Outlet Node,  !- Temperature Setpoint Node Name
    B11 DHW Tank Use Side Pump-Tank Node;  !- Pump Outlet Node Name

Branch,
    B11 DHW Tank Use Outlet Branch,  !- Name
    0,                       !- Maximum Flow Rate {m3/s}
    ,                        !- Pressure Drop Curve Name
    WaterHeater:Mixed,       !- Component 1 Object Type
    B11 DHW Heater,              !- Component 1 Name
    B11 DHW Heater Inlet Node,   !- Component 1 Inlet Node Name
    B11 DHW Heater Outlet Node,  !- Component 1 Outlet Node Name
    ACTIVE;                  !- Component 1 Branch Control Type

WaterHeater:Mixed,
    B11 DHW Heater,              !- Name
    0.00568,                 !- Tank Volume {m3}
    B11 DHW Heater Setpoint Temp Schedule,  !- Setpoint Temperature Schedule Name
    ,                        !- Deadband Temperature Difference {deltaC}
    82.2222,                 !- Maximum Temperature Limit {C}
    MODULATE,                !- Heater Control Type
    4500,                   !- Heater Maximum Capacity {W}
    0,                        !- Heater Minimum Capacity {W}
    ,                        !- Heater Ignition Minimum Flow Rate {m3/s}
    ,                        !- Heater Ignition Delay {s}
    ELECTRICITY,             !- Heater Fuel Type
    0.90,                    !- Heater Thermal Efficiency
    ,                        !- Part Load Factor Curve Name
    ,                        !- Off Cycle Parasitic Fuel Consumption Rate {W}
    ,                        !- Off Cycle Parasitic Fuel Type
    ,                        !- Off Cycle Parasitic Heat Fraction to Tank
    ,                        !- On Cycle Parasitic Fuel Consumption Rate {W}
    ,                        !- On Cycle Parasitic Fuel Type
    ,                        !- On Cycle Parasitic Heat Fraction to Tank
    SCHEDULE,                !- Ambient Temperature Indicator
    Ambient Temp Schedule,   !- Ambient Temperature Schedule Name
    ,                        !- Ambient Temperature Zone Name
    ,                        !- Ambient Temperature Outdoor Air Node Name
    1.0,                     !- Off Cycle Loss Coefficient to Ambient Temperature {W/K}
    1,                       !- Off Cycle Loss Fraction to Zone
    1.0,                     !- On Cycle Loss Coefficient to Ambient Temperature {W/K}
    1,                       !- On Cycle Loss Fraction to Zone
    ,                        !- Peak Use Flow Rate {m3/s}
    ,                        !- Use Flow Rate Fraction Schedule Name
    ,                        !- Cold Water Supply Temperature Schedule Name
    B11 DHW Heater Inlet Node,   !- Use Side Inlet Node Name
    B11 DHW Heater Outlet Node,  !- Use Side Outlet Node Name
    1.0,                     !- Use Side Effectiveness
    ,                        !- Source Side Inlet Node Name
    ,                        !- Source Side Outlet Node Name
    ,                        !- Source Side Effectiveness
    autosize;                !- Use Side Design Flow Rate {m3/s}

Schedule:Compact,
    B11 DHW Heater Setpoint Temp Schedule,  !- Name
    Any Number,              !- Schedule Type Limits Name
    THROUGH: 12/31,          !- Field 1
    FOR: AllDays,            !- Field 2
    UNTIL: 24:00, 45;        !- Field 4

